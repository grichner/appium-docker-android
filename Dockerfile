FROM opensuse/tumbleweed
#:bionic-20200311 

ENV DEBIAN_FRONTEND=noninteractive

## ENV SDK_VERSION=sdk-tools-linux-3859397 \
##https://dl.google.com/android/repository/commandlinetools-linux-11076708_latest.zip ##sdkmanager V12.0
##https://dl.google.com/android/repository/commandlinetools-linux-10406996_latest.zip ##sdkmanager V11.0
ENV SDK_VERSION=commandlinetools-linux-11076708_latest \
    ANDROID_BUILD_TOOLS_VERSION=34.0.0 \
    ANDROID_FOLDER_NAME=cmdline-tools/latest \
    ANDROID_HOME=/root \
##    APPIUM_VERSION=2.5.4 \ ##latest
    ATD_VERSION=1.2 \
    APPIUM_SKIP_CHROMEDRIVER_INSTALL=true
ENV ANDROID_DOWNLOAD_PATH=${ANDROID_HOME}/Downloads
ENV ANDROID_TOOL_HOME=${ANDROID_HOME}/${ANDROID_FOLDER_NAME}

#=============
# Set WORKDIR
#=============
WORKDIR /root

#==================
# General Packages
#------------------
# openjdk-8-jdk
#   Java
# ca-certificates
#   SSL client
# tzdata
#   Timezone
# zip
#   Make a zip file
# unzip
#   Unzip zip file
# curl
#   Transfer data from or to a server
# wget
#   Network downloader
# libqt5webkit5
#   Web content engine (Fix issue in Android)
# libgconf-2-4
#   Required package for chrome and chromedriver to run on Linux
# xvfb
#   X virtual framebuffer
# gnupg
#   Encryption software. It is needed for nodejs
# salt-minion
#   Infrastructure management (client-side)
#==================
RUN zypper in -y java-17-openjdk-headless \
    ca-certificates \
    timezone \
    zip \
    unzip \
    curl \
    wget \
    xvfb-run \
    gnupg \
    libvulkan_intel libvulkan_lvp libvulkan_radeon
    
## RUN zypper in -y salt-minion
##    libQt6WebEngineCore6
#    libgconfmm-2_6-1     
##RUN zypper addrepo https://download.opensuse.org/repositories/KDE:Qt5/openSUSE_Factory/KDE:Qt5.repo && \
##    zypper --no-gpg-checks refresh && \
##    zypper install -y libQt5WebKit5
    
#RUN apt-get -qqy update && \
#    apt-get -y upgrade && \
#    apt-get -qqy --no-install-recommends install \
#    openjdk-8-jdk \
#    ca-certificates \
#    tzdata \
#    zip \
#    unzip \
#    curl \
#    wget \
#    libqt5webkit5 \
#    libgconf-2-4 \
#    xvfb \
#    gnupg \
#    salt-minion \
#  && rm -rf /var/lib/apt/lists/*

#===============
# Set JAVA_HOME
#===============
ENV JAVA_HOME="/usr/lib64/jvm/jre" \
    PATH=$PATH:$JAVA_HOME/bin

#=====================
#=====================
ARG ANDROID_PLATFORM_VERSION="android-34"

RUN wget -O tools.zip https://dl.google.com/android/repository/${SDK_VERSION}.zip && \
    mkdir -p cmdline-tools && \
    unzip -d cmdline-tools tools.zip && rm tools.zip && \
    mv cmdline-tools/cmdline-tools ${ANDROID_FOLDER_NAME} && \
    chmod a+x -R $ANDROID_HOME && \
    chown -R root:root $ANDROID_HOME

ENV PATH=$PATH:$ANDROID_TOOL_HOME:$ANDROID_TOOL_HOME/bin

# https://askubuntu.com/questions/885658/android-sdk-repositories-cfg-could-not-be-loaded
RUN mkdir -p ~/.android && \
    touch ~/.android/repositories.cfg && \
    echo y | sdkmanager "platform-tools" && \
    echo y | sdkmanager "build-tools;$ANDROID_BUILD_TOOLS_VERSION" && \
    echo y | sdkmanager "platforms;$ANDROID_PLATFORM_VERSION"

ENV PATH=$PATH:$ANDROID_HOME/build-tools

#====================================
# Install latest nodejs, npm, appium
# Using this workaround to install Appium -> https://github.com/appium/appium/issues/10020 -> Please remove this workaround asap
#====================================
RUN zypper in -y nodejs22 npm22
##RUN npm install -g appium@${APPIUM_VERSION} --unsafe-perm=true --allow-root
RUN npm install -g appium --unsafe-perm=true --allow-root

RUN zypper in -y python311
RUN appium driver install uiautomator2
RUN mkdir -p /root/.appium/node_modules/appium-uiautomator2-driver/node_modules/appium-chromedriver/chromedriver/linux
RUN ln -s /root/chromedriver /root/.appium/node_modules/appium-uiautomator2-driver/node_modules/appium-chromedriver/chromedriver/linux/chromedriver
### images plugin does not work with python311
## RUN appium plugin install images
RUN npm cache verify 
RUN zypper clean --all
    
    

#RUN curl -sL https://deb.nodesource.com/setup_14.x | bash && \
#    apt-get -qqy install nodejs && \
#    npm install -g appium@${APPIUM_VERSION} --unsafe-perm=true --allow-root && \
#    exit 0 && \
#    npm cache clean && \
#    apt-get remove --purge -y npm && \
#    apt-get autoremove --purge -y && \
#    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
#    apt-get clean

#================================
# APPIUM Test Distribution (ATD)
#================================
## removed because of too many CVEs
##RUN wget -nv -O RemoteAppiumManager.jar "https://github.com/AppiumTestDistribution/ATD-Remote/releases/download/${ATD_VERSION}/RemoteAppiumManager-${ATD_VERSION}.jar"

#==================================
# Fix Issue with timezone mismatch
#==================================
ENV TZ="Europe/Berlin"
RUN rm -f /etc/localtime && \
    ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN echo "\
export LC_ALL=de_DE.utf8 \
export LANG=de_DE.utf8 \
">/etc/profile.d/LOCALE.sh

#===============
# Expose Ports
#---------------
# 4723
#   Appium port
# 4567
#   ATD port
#===============
EXPOSE 4723
EXPOSE 4567

#====================================================
# Scripts to run appium and connect to Selenium Grid
#====================================================
COPY entry_point.sh \
     generate_config.sh \
     wireless_connect.sh \
     wireless_autoconnect.sh \
     /root/

RUN chmod +x /root/entry_point.sh && \
    chmod +x /root/generate_config.sh && \
    chmod +x /root/wireless_connect.sh && \
    chmod +x /root/wireless_autoconnect.sh && \
    mkdir -p ${ANDROID_DOWNLOAD_PATH}
    

#========================================
# Run xvfb and appium server
#========================================
CMD /root/wireless_autoconnect.sh && /root/entry_point.sh
